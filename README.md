# Avery-React-App

## Goal

create a react app that can pull a xkcd comic from the web and display it within the app.

### Requirements

---
UI Elements:

- container to display the image
- left/right arrows to move to previous/next days respectivley.
- input form fields to take day/month/year
- button for execution
- button for random

*structure the ui in a way that makes sense to you, and be ready to give insight into your choices*

---
Backend:

- call to get xkcd comic for a given day
- call to get random xkcd comic (must not repeat for the same user in the same session)

---
Resources:

- [xkcd api info link](https://xkcd.com/json.html)
- [create react app](https://github.com/facebookincubator/create-react-app)

---
Caveats:

- xkcd returns a comic based on  internal id path parameters, not dates. You must devise some logic to support this in your app.
- noone knows everything, if you dont know something ask, either one of us or the [smartest person in the world](https://google.com)

---
Future:

- Implement material design for [react](https://github.com/mui-org/material-ui/tree/master/examples/create-react-app)
- Implement [redux](https://auth0.com/blog/redux-practical-tutorial/), [redux-sagas, router](https://auth0.com/blog/beyond-create-react-app-react-router-redux-saga-and-more/)
- Unit testing with [baretest](https://github.com/volument/baretest)
- Documentation with [markdown](https://www.markdownguide.org/)
